import os
import subprocess
import time

simulator = 'simulator'
message = '123'


def read_file(file_name):
    with open(f'{simulator}/{file_name}', 'r') as file:
        return str(file.read()).replace('\n', '')


def list_files_in_directory(directory):
    return os.listdir(directory)


def main():
    scripts = ['captura.py', 'monitor.py', 'output.py']
    script_path = ''
    # Verifique a existência de todos os scripts
    for script in scripts:
        if not os.path.exists(os.path.join(script_path, script)):
            print("Script", script, "não encontrado.")
            exit(1)

    # Execute o script 'input.py'
    subprocess.run(['python3', os.path.join(script_path, 'input.py'), message])

    # Inicie os subprocessos 'captura.py', 'monitor.py', 'output.py'
    processes = []
    for _script in scripts:
        p = subprocess.Popen(['python3', os.path.join(script_path, _script)])
        processes.append(p)

    # Aguarde alguns segundos
    time.sleep(10)

    # Enviar um sinal para finalizar os processos
    for p in processes:
        p.terminate()

    files = list_files_in_directory(directory=simulator)

    # Verifique a saída
    print(f'arquivos {files}')

    for _script in scripts:
        with open(os.path.join(simulator, f'{_script[:-3]}'), 'r') as f:
            value = str(f.read()).replace('\n', '')
            print(f"Saída de {_script}: {value}")

    assert read_file(file_name="captura") == message
    assert read_file(file_name="output") == f'{message}:output'
    assert read_file(file_name="monitor") == f'{message}:monitor{message}:output'


if __name__ == '__main__':
    main()
