import os

import pika
from pika.exceptions import ChannelClosedByBroker

# Configurações do RabbitMQ
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT')
RABBITMQ_USER = os.getenv('RABBITMQ_USER')
RABBITMQ_PASSWORD = os.getenv('RABBITMQ_PASSWORD')
EXCHANGE_NAME = 'topic.captura.event'
QUEUES = ['queue.captura.input', 'queue.captura.output', 'queue.captura.event']


def check_queue_exists(channel, queue_name):
    try:
        channel.queue_declare(queue=queue_name, passive=True)  # passive=True apenas verifica a existência
        print(f"Fila '{queue_name}' existe.")
    except ChannelClosedByBroker as e:
        print(f"Fila '{queue_name}' não existe.")
        raise e


def check_exchange_exists(channel, exchange_name):
    try:
        channel.exchange_declare(exchange=exchange_name, passive=True)
        print(f"Exchange '{exchange_name}' existe.")
    except ChannelClosedByBroker as e:
        print(f"Exchange '{exchange_name}' não existe.")
        raise e


def main():
    # Conectar ao RabbitMQ
    credentials = pika.PlainCredentials(RABBITMQ_USER, RABBITMQ_PASSWORD)
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(RABBITMQ_HOST, RABBITMQ_PORT, RABBITMQ_USER, credentials))
    channel = connection.channel()

    # Verificar a existência das filas
    for queue_name in QUEUES:
        check_queue_exists(channel, queue_name)

    # Verificar a existência da exchange
    check_exchange_exists(channel, EXCHANGE_NAME)

    # Fechar a conexão
    connection.close()


if __name__ == '__main__':
    main()
