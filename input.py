import sys

from core.build import create_channel
from core.channel import Channel


def get_sys_message(default_message: str = "Message") -> str:
    if len(sys.argv) > 1:
        return sys.argv[1]
    else:
        return default_message


if __name__ == '__main__':
    channel: Channel = create_channel(process_name='Initial')
    channel.insert_queue_event(message=get_sys_message(), queue='queue.captura.input')
