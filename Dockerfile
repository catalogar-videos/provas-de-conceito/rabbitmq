FROM rabbitmq:3-management-alpine

RUN adduser -D simulator

WORKDIR /home/simulator

RUN apk update

RUN apk add python3

RUN apk add py3-pip

RUN python3 -m venv .venv

RUN .venv/bin/pip install pika Unidecode

COPY scripts .

COPY *.py .

COPY core core

ENTRYPOINT ["bash", "init.sh"]
