import os

import unidecode

from core.channel import Channel


def _clean(value: str) -> str:
    return unidecode.unidecode(str(value).replace(' ', ''))


def _get_metadata() -> dict[str, str]:
    metadata: dict[str, str] = {
        'host': _clean(os.getenv('RABBITMQ_HOST')),
        'port': _clean(os.getenv('RABBITMQ_PORT')),
        'user': _clean(os.getenv('RABBITMQ_USER')),
        'password': _clean(os.getenv('RABBITMQ_PASSWORD'))
    }
    print(metadata)
    return metadata


def create_channel(process_name: str) -> Channel:
    print('Process : {}'.format(process_name))
    return Channel(_metadata=_get_metadata())
