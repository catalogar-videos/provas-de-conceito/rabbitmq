import os

import unidecode


def write_in_file(filename, content):
    simulator_folder = "simulator"
    os.makedirs(name=simulator_folder, exist_ok=True)
    with open(f'{simulator_folder}/{filename}', 'a') as f:
        f.write(unidecode.unidecode(content))
        f.write("\n")
