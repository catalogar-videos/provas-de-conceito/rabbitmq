import pika


class Channel:

    def __init__(self, _metadata: dict[str, str]):
        credentials = pika.PlainCredentials(_metadata['user'], _metadata['password'])
        parameters = pika.ConnectionParameters(host=_metadata['host'], port=int(_metadata['port']),
                                               virtual_host=_metadata['user'], credentials=credentials)
        self.connection = pika.BlockingConnection(parameters)
        self.rabbit = self.connection.channel()

    def insert_queue_event(self, message, queue: str):
        self._insert_message(exchange='', routing_key=queue, message=message)

    def insert_exchange_event(self, message, exchange: str, routing_key: str = ''):
        self._insert_message(exchange=exchange, routing_key=routing_key, message=message)

    def _insert_message(self, message, exchange: str, routing_key: str):
        print('[Exchange {}, Key/Queue {}] Inserting message : {}'.format(exchange, routing_key, message))
        self.rabbit.basic_publish(exchange=exchange, routing_key=routing_key, body=message)

    def create_queue(self, exchange: str):
        queue = exchange
        self.rabbit.queue_declare(queue=queue, durable=True)
        self.rabbit.queue_bind(exchange=exchange, queue=queue, routing_key='#')
        return queue

    def get_event(self, queue: str, message_callback):
        self.rabbit.basic_consume(queue=queue, auto_ack=True, on_message_callback=message_callback)
        self.rabbit.start_consuming()

    def __del__(self):
        self.connection.close()
        print("Closing connection")
