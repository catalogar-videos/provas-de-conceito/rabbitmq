from core.build import create_channel
from core.channel import Channel
from core.write import write_in_file

if __name__ == '__main__':

    channel: Channel = create_channel(process_name='Captura')

    queue: str = 'queue.captura.input'


    def callback_event(ch, method, properties, body, json=None):
        message = str(body.decode('utf-8'))
        print(f"Captura : {message}")
        write_in_file(filename="captura", content=message)
        channel.insert_exchange_event(message=f'{message}:monitor', exchange='topic.captura.event')
        channel.insert_exchange_event(message=f'{message}:output', exchange='topic.captura.event',
                                      routing_key='captura.output')


    try:
        channel.get_event(queue=queue, message_callback=callback_event)
    except KeyboardInterrupt:
        print("Interrupted")
