from core.build import create_channel
from core.channel import Channel
from core.write import write_in_file

if __name__ == '__main__':

    channel: Channel = create_channel(process_name='Output Monitor')

    queue: str = 'queue.captura.output'


    def callback_event(ch, method, properties, body, json=None):
        message = str(body.decode('utf-8'))
        print(f"Event : {message}")
        write_in_file(filename="output", content=message)


    try:
        channel.get_event(queue=queue, message_callback=callback_event)
    except KeyboardInterrupt:
        print("Interrupted")
